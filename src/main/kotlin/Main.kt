package trainreservationsystem

import io.ktor.application.*
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.time.LocalDate

data class Ticket(val seatNumber: Int, val coachNumber: Int, val date: LocalDate = LocalDate.now())
data class Person(val name: String, val age: String)
data class Train(val trainNumber : Int, val numberOfCoach : Int, val seats :List<Int>)


fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(Routing) {
        get("/") {
            call.respondText("Tickets are booked", ContentType.Text.Html)
        }
    }
}

fun main(args: Array<String>) {
    embeddedServer(Netty, 8080, watchPaths = listOf("MainKT"), module = Application::module).start()
}